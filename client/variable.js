export const config = {
    baseUrl: "http://localhost:3400",
    sanpham:{
        getAll: "http://localhost:3400/sp",
        getByTenSP:"http://localhost:3400/sp",
        getByTH:"http://localhost:3400/sp/getTH"
    },
    thuonghieu:{
        getAll:"http://localhost:3400/th"
    },
    mausac:{
        getByMaSP:"http://localhost:3400/mausac"
    },
    khachhang:{
        checkLogin:"http://localhost:3400/kh/checkLogin",
        register:"http://localhost:3400/kh/register"
    },
    giohang:"http://localhost:3400/cart"
}