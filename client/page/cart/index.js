import { config } from "../../variable.js";

export default ($scope, $rootScope, $http, $routeParams) => {
    $http.get(`${config.giohang}/${$routeParams.makh}`).then((result) => {
        $scope.cart = result.data;
        $scope.cart = $scope.cart.map(s => { return { ...s, isChon: false } });
    }).catch((error) => {
        console.log(error);
    })
    $scope.toOrder = () => {
        localStorage.setItem("cart", JSON.stringify($scope.cart.filter(s => s.isChon)));
    }
    $scope.delete = async (value) => {
        await $http.delete(`${config.giohang}/delete/${value}`).then((result) => {
            console.log(result);
        });
        window.location.reload();
    }
    $scope.log = async () => {
        console.log($scope.quantity);
    }
    $scope.changeNumber = async(magiohang, soluong, math) => {
        switch (math) {
            case "-":
                soluong -= 1;
                break;
            case "+":
                soluong += 1;
                break;
        }
        await $http.put(`${config.giohang}/updateSoLuong?magiohang=${magiohang}&soluong=${soluong}`).then(s=>{
            console.log(s);
        });
        await $http.get(`${config.giohang}/${$routeParams.makh}`).then((result) => {
            $scope.cart = result.data;
            $scope.cart = $scope.cart.map(s => { return { ...s, isChon: false } });
        }).catch((error) => {
            console.log(error);
        })
    }
}