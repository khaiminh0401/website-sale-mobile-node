import { config } from "../../variable.js"

export default async ($scope, $http,$routeParams) =>{
    $scope.thuonghieu = await $http.get(config.thuonghieu.getAll).then((res)=>{
        return res.data;
    })
    if($routeParams.th == 'all'){
        $scope.sanpham = await $http.get(config.sanpham.getAll).then(res=>{
            return res.data;
        })
    }else{
        $scope.sanpham = await $http.get(`${config.sanpham.getByTH}/${$routeParams.th}`).then(res=>{
            return res.data;
        })
    }
    $scope.th = $routeParams.th;

    $scope.page = Math.ceil($scope.sanpham.length/6);
    $scope.getIndexPage = (value) =>{
        return new Array(value);
    }
    $scope.begin = ($routeParams.page-1)*6;
}