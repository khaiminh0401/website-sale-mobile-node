export default ($scope,$rootScope) =>{
    if(localStorage.getItem("user")!=undefined){
        $rootScope.khachhang = JSON.parse(localStorage.getItem("user"));
    }
    $scope.logout = () =>{
        delete $rootScope.khachhang;
        localStorage.removeItem("user");
        window.location.href = window.location.href;
    }
}