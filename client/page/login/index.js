import { config } from "../../variable.js";

export default ($scope,$rootScope,$http) =>{
    $scope.login = (e) =>{
        console.log(e);
        if($scope.email == null && $scope.matkhau == null){
            console.log(1);
            window.location.href = window.location.href;
            $scope.message = "Đăng nhập không thành công";
            return;
        }
        $scope.input = {
            email:$scope.email,
            matkhau: $scope.matkhau
        }
        $http.get(config.khachhang.checkLogin,{params:$scope.input}).then((result) => {
            console.log(result.data);
            if(result.data!=null){
                $rootScope.khachhang = result.data;
                localStorage.setItem("user",JSON.stringify(result.data));
                window.location.href= "#";
                delete $scope.email;
                delete $scope.matkhau;
            }else{
                $scope.message = "Tài khoản hoặc mật khẩu không tồn tại";
            }
        }).catch((err) => {
            console.log(err);
        });
    }
}