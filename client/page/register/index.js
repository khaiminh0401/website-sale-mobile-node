import { config } from "../../variable.js";

export default ($scope,$http) =>{
    $scope.submit = (e) =>{
        console.log(e);
        if($scope.tenkh == null && $scope.email ==null && $scope.gioitinh ==null && $scope.diachi==null && $scope.sdt==null){
            $scope.message = "Đăng nhập không thành công";
            window.location.href = window.location.href;
            return;
        }
        const obj = {
            tenkh: $scope.tenkh,
            email:$scope.email,
            gioitinh:$scope.gioitinh,
            diachi:$scope.diachi,
            sdt:$scope.sdt,
            matkhau:$scope.matkhau
        }
        $.post(config.khachhang.register,obj).then(res =>{
            if(res == 0){
                window.location.href = "#!";
            }
        })
    }
}