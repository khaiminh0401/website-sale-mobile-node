import { config } from "../../variable.js";

export default async($scope,$rootScope,$http,$routeParams) =>{
    $scope.state = {
        isRam:false
    }
    $scope.sanpham = await $http.get(`${config.sanpham.getByTenSP}/${$routeParams.tensp}`).then((result) => {
        return result.data[0];
        // $http.get(`${config.mausac.getByMaSP}/${$scope.sanpham.masp}`).then(res=>{
        //     $scope.sanpham = {...$scope.sanpham,mausac:res.data}
        // })
    }).catch((err) => {
        console.log(err);
    });
    $scope.clickColor = (index) =>{
        $scope.indexColor = index;
        // console.log(index);
    }
    $scope.addToCart = async() =>{
        const cart = {
            masp: $scope.sanpham.masp,
            mamau: $scope.sanpham.colors[$scope.indexColor].mamau,
            soluong: 1,
            makh: $rootScope.khachhang.makh
          };
        await $http.post(`${config.giohang}/insert`,cart).then(s=>{
            window.location.href = "#!cart/"+$rootScope.khachhang.makh;
        });
    }
}