import { config } from "../../variable.js";
const homeCtrl = ($scope,$http) =>{
    $http.get(config.sanpham.getAll).then((res)=>{
        $scope.sanpham = res.data;
    })
}
export default homeCtrl;