import CustomConfig from "./config.js";
import controller from "./controller.js";
var app = angular.module("myapp",['ngRoute']);

app.config(CustomConfig);

app.run(function ($rootScope,$timeout) {  
    $rootScope.$on("$routeChangeStart",function(){

        $rootScope.loading = true;
    })
    $rootScope.$on("$routeChangeSuccess",function(){
        $timeout(()=>{
            $rootScope.loading = false;
        },800);
    })
    $rootScope.$on("routeChangeError",function () {  
        $rootScope.loading = false;
        alert("lỗi không tải template được");
    })
});
controller(app);