import homeCtrl from './page/home/index.js';
import loginCtrl from './page/login/index.js';
import dtddCtrl from './page/dtdd/index.js';
import detailCtrl from './page/detail/index.js';
import cartCtrl from './page/cart/index.js';
import navbar from './page/include/navbar.js';
import myCtrl from './myCtrl.js';
import profile from './page/profile/index.js';
import order from './page/order/index.js';
import changepassword from './page/changepassword/index.js';
import registerCtrl from './page/register/index.js';
export default (app) =>{
    app
    .controller("myCtrl",myCtrl)
    .controller("homeCtrl",homeCtrl)
    .controller("loginCtrl",loginCtrl)
    .controller("dtddCtrl",dtddCtrl)
    .controller("detailCtrl",detailCtrl)
    .controller("cartCtrl",cartCtrl)
    .controller("navCtrl",navbar)
    .controller("profileCtrl",profile)
    .controller("orderCtrl",order)
    .controller("changePasswordCtrl",changepassword)
    .controller("registerCtrl",registerCtrl)
}   